<?php
/**
 * Image Sizes.
 *
 * @since  2.2.0
 *
 * @global array $post WP post object.
 */

/**
 * Add Suport Image Size
 */

add_image_size( 'img-gallery', 400, 400, true ); // Imagem
add_image_size( 'img-gallery-3col', 210, 210, true ); // Imagem

/*

add_image_size( 'img-news-home', 350, 300, true ); // Imagem

add_image_size( 'img-gallery-page', 231, 231, true ); // Imagem
add_image_size( 'img-experience-home', 759, 477, true ); // Imagem
add_image_size( 'img-map-home', 779, 356, true ); // Imagem
add_image_size( 'img-background-footer', 1413, 406, true ); // Imagem
add_image_size( 'img-background-about', 1368, 281, true ); // Imagem
add_image_size( 'img-team-about', 253, 258, true ); // Imagem
add_image_size( 'img-thumb-cayman', 253, 253, true ); // Imagem
add_image_size( 'img-thumb-destination-page', 253, 125, true ); // Imagem
add_image_size( 'img-thumb-experience', 254, 400, true ); // Imagem
add_image_size( 'img-thumb-out-team', 145, 145, true ); // Imagem
add_image_size( 'img-top-page', 1369, 233, true ); // Imagem
add_image_size( 'img-top-page-booking', 1369, 175, true ); // Imagem
//add_image_size( 'img-top-post', 1110, 470, true ); // Imagem
add_image_size( 'img-feature-post', 725, 472, true ); // Imagem
add_image_size( 'img-category-grand-cayman', 1110, 400, true ); // Imagem
add_image_size( 'img-banner-internal', 1110, 235, true ); // Imagem
add_image_size( 'img-icon-booking', 115, 115, true ); // Imagem
add_image_size( 'img-boat-table', 459, 181, true ); // Imagem
add_image_size( 'img-boat-gallery', 555, 404, true ); // Imagem
add_image_size( 'img-single-boat-gallery', 230, 230, true ); // Imagem
add_image_size( 'img-single-experience-gallery', 277, 277, true ); // Imagem
add_image_size( 'img-bg-video', 632, 435, true ); // Imagem
add_image_size( 'img-sidebar-travel', 350, 215, true ); // Imagem
add_image_size( 'img-our-team-home', 500, 225, true ); // Imagem
add_image_size( 'img-banner-responsive-home', 991, 438, true ); // Imagem
add_image_size( 'img-page-review', 542, 320, true ); // Imagem
*/