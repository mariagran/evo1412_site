<?php
/**
 * Sidebars.
 *
 * @since  2.2.0
 *
 * @param  string $homepage  Homepage name.
 *
 * @return string            HTML of breadcrumbs.
 */


/*Register Sidebar*/
/*
add_action( 'widgets_init', 'theme_slug_widgets_init_1' );
function theme_slug_widgets_init_1() {
    register_sidebar( array(
        'name' => __( 'Home Col 1', 'vou-para-tour' ),
        'id' => 'sidebar-home-1',
        'description' => __( 'Widgets desta área serão exibidos na página Home.', 'vou-para-tour' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="widgettitle">',
	'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'theme_slug_widgets_init_12' );
function theme_slug_widgets_init_12() {
    register_sidebar( array(
        'name' => __( 'Home Col 2', 'vou-para-tour' ),
        'id' => 'sidebar-home-2',
        'description' => __( 'Widgets desta área serão exibidos na página Home.', 'vou-para-tour' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="widgettitle">',
	'after_title'   => '</h2>',
    ) );
}


add_action( 'widgets_init', 'theme_slug_widgets_init_3' );
function theme_slug_widgets_init_3() {
    register_sidebar( array(
        'name' => __( 'Informações Col 1', 'vou-para-tour' ),
        'id' => 'sidebar-info-1',
        'description' => __( 'Widgets desta área serão exibidos na área informações das páginas.', 'vou-para-tour' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="widgettitle">',
	'after_title'   => '</h2>',
    ) );
}

add_action( 'widgets_init', 'theme_slug_widgets_init_4' );
function theme_slug_widgets_init_4() {
    register_sidebar( array(
        'name' => __( 'Informações Col 2', 'vou-para-tour' ),
        'id' => 'sidebar-info-2',
        'description' => __( 'Widgets desta área serão exibidos na área informações das páginas.', 'vou-para-tour' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="widgettitle">',
	'after_title'   => '</h2>',
    ) );
}

add_action( 'widgets_init', 'theme_slug_widgets_init_5' );
function theme_slug_widgets_init_5() {
    register_sidebar( array(
        'name' => __( 'Informações Col 3', 'vou-para-tour' ),
        'id' => 'sidebar-info-3',
        'description' => __( 'Widgets desta área serão exibidos na área informações das páginas.', 'vou-para-tour' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="widgettitle">',
	'after_title'   => '</h2>',
    ) );
}

add_action( 'widgets_init', 'theme_slug_widgets_init_6' );
function theme_slug_widgets_init_6() {
    register_sidebar( array(
        'name' => __( 'Sidebar Blog', 'vou-para-tour' ),
        'id' => 'sidebar-blog',
        'description' => __( 'Widgets desta área serão exibidos nas páginas de blog.', 'vou-para-tour' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="widgettitle">',
	'after_title'   => '</h2>',
    ) );
}

add_action( 'widgets_init', 'theme_slug_widgets_init_7' );
function theme_slug_widgets_init_7() {
    register_sidebar( array(
        'name' => __( 'Contato', 'vou-para-tour' ),
        'id' => 'sidebar-contato',
        'description' => __( 'Widgets desta área serão exibidos no rodapé das páginas.', 'vou-para-tour' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="widgettitle">',
	'after_title'   => '</h2>',
    ) );
}


add_action( 'widgets_init', 'theme_slug_widgets_init_1' );
function theme_slug_widgets_init_1() {
    register_sidebar( array(
        'name' => __( 'Blog', 'four-tour' ),
        'id' => 'blog-sidebar',
        'description' => __( 'Widgets in this area will be displayed in sidebar blog.', 'evo' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2 class="widgettitle">',
    'after_title'   => '</h2>',
    ) );
}
*/

add_action( 'widgets_init', 'theme_slug_widgets_init_8' );
function theme_slug_widgets_init_8() {
    register_sidebar( array(
        'name' => __( 'Footer Col 1', 'vou-para-tour' ),
        'id' => 'sidebar-footer-1',
        'description' => __( 'Widgets in this area will be displayed in the footer of the pages.', 'evo' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h6 class="widgettitle">',
	'after_title'   => '</h6>',
    ) );
}

add_action( 'widgets_init', 'theme_slug_widgets_init_9' );
function theme_slug_widgets_init_9() {
    register_sidebar( array(
        'name' => __( 'Footer Col 2', 'evo' ),
        'id' => 'sidebar-footer-2',
        'description' => __( 'Widgets in this area will be displayed in the footer of the pages.', 'evo' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h6 class="widgettitle">',
	'after_title'   => '</h6>',
    ) );
}

add_action( 'widgets_init', 'theme_slug_widgets_init_10' );
function theme_slug_widgets_init_10() {
    register_sidebar( array(
        'name' => __( 'Footer Col 3', 'evo' ),
        'id' => 'sidebar-footer-3',
        'description' => __( 'Widgets in this area will be displayed in the footer of the pages.', 'evo' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h6 class="widgettitle">',
	'after_title'   => '</h6>',
    ) );
}


add_action( 'widgets_init', 'theme_slug_widgets_init_11' );
function theme_slug_widgets_init_11() {
    register_sidebar( array(
        'name' => __( 'Footer Col 4', 'evo' ),
        'id' => 'sidebar-footer-4',
        'description' => __( 'Widgets in this area will be displayed in the footer of the pages.', 'evo' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h6 class="widgettitle">',
	'after_title'   => '</h6>',
    ) );
}

add_action( 'widgets_init', 'theme_slug_widgets_init_14' );
function theme_slug_widgets_init_14() {
    register_sidebar( array(
        'name' => __( 'Reviews Home', 'evo' ),
        'id' => 'sidebar-reviews-home',
        'description' => __( 'Widgets desta área serão exibidos na área reviews da hoempage.', 'veditalia' ),
        'before_widget' => '<div id="%1$s" class="reviews-home flexslider widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2 class="widgettitle">',
    'after_title'   => '</h2>',
    ) );
}
/*
add_action( 'widgets_init', 'theme_slug_widgets_init_12' );
function theme_slug_widgets_init_12() {
    register_sidebar( array(
        'name' => __( 'Footer Col 5', 'evo' ),
        'id' => 'sidebar-footer-5',
        'description' => __( 'Widgets in this area will be displayed in the footer of the pages.', 'evo' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h6 class="widgettitle">',
	'after_title'   => '</h6>',
    ) );
}
add_action( 'widgets_init', 'theme_slug_widgets_init_13' );
function theme_slug_widgets_init_13() {
    register_sidebar( array(
        'name' => __( 'Footer Col 6', 'evo' ),
        'id' => 'sidebar-footer-6',
        'description' => __( 'Widgets in this area will be displayed in the footer of the pages.', 'evo' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h6 class="widgettitle">',
	'after_title'   => '</h6>',
    ) );
}

add_action( 'widgets_init', 'theme_slug_widgets_init_14' );
function theme_slug_widgets_init_14() {
    register_sidebar( array(
        'name' => __( 'Info Footer Col 1', 'evo' ),
        'id' => 'sidebar-info-footer-1',
        'description' => __( 'Widgets in this area will be displayed in the footer of the pages.', 'evo' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h6 class="widgettitle">',
	'after_title'   => '</h6>',
    ) );
}
add_action( 'widgets_init', 'theme_slug_widgets_init_15' );
function theme_slug_widgets_init_15() {
    register_sidebar( array(
        'name' => __( 'Info Footer Col 2', 'evo' ),
        'id' => 'sidebar-info-footer-2',
        'description' => __( 'Widgets in this area will be displayed in the footer of the pages.', 'evo' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h6 class="widgettitle">',
	'after_title'   => '</h6>',
    ) );
}
add_action( 'widgets_init', 'theme_slug_widgets_init_16' );
function theme_slug_widgets_init_16() {
    register_sidebar( array(
        'name' => __( 'Info Footer Col 3', 'evo' ),
        'id' => 'sidebar-info-footer-3',
        'description' => __( 'Widgets in this area will be displayed in the footer of the pages.', 'evo' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h6 class="widgettitle">',
	'after_title'   => '</h6>',
    ) );
}

*/

/*
add_action( 'widgets_init', 'theme_slug_widgets_init_14' );
function theme_slug_widgets_init_14() {
    register_sidebar( array(
        'name' => __( 'Blog Rodapé', 'vou-para-tour' ),
        'id' => 'blog-rodape',
        'description' => __( 'Widgets desta área serão exibidos no radapé da lateral de blog.', 'vou-para-tour' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2 class="widgettitle">',
    'after_title'   => '</h2>',
    ) );
}

add_action( 'widgets_init', 'theme_slug_widgets_init_15' );
function theme_slug_widgets_init_15() {
    register_sidebar( array(
        'name' => __( 'Tour Social', 'vou-para-tour' ),
        'id' => 'sidebar-tour',
        'description' => __( 'Widgets desta área serão exibidos nas páginas de Tours.', 'vou-para-tour' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2 class="widgettitle">',
    'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'theme_slug_widgets_init_16' );
function theme_slug_widgets_init_16() {
    register_sidebar( array(
        'name' => __( 'Tour Contato', 'vou-para-tour' ),
        'id' => 'tour-contato',
        'description' => __( 'Widgets desta área serão exibidos na lateral ddo tour.', 'vou-para-tour' ),
        'before_widget' => '<div id="%1$s" class="contato-sidebar-text widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2 class="widgettitle">',
    'after_title'   => '</h2>',
    ) );
}
*/