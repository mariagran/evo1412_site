<?php 

moove_Kirki::add_config( 'evo', array(
    'capability'    => 'edit_theme_options',
    'option_type'   => 'theme_mod',
) );

moove_Kirki::add_panel( 'panel_contact', array(
    'priority'    => 21,
    'title'       => __( 'Contato', 'evo' ),
    'description' => __( 'Contato', 'evo' ),
) );

moove_Kirki::add_section( 'simulacao_section', array(
    'title'          => __( 'Simulacao', 'evo' ),
    'description'    => __( 'Insira o texto e link da simulação', 'evo' ),
    'panel'          => 'panel_contact', // Not typically needed.
    'priority'       => 160,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '', // Rarely needed.
) );
moove_Kirki::add_field( 'my_config', array(
    'type'     => 'text',
    'settings' => 'simulacao',
    'label'    => __( 'Texto simulação', 'evo' ),
    'section'  => 'simulacao_section',
    'default'  => esc_attr__( '', 'evo' ),
    'priority' => 10,
) );
moove_Kirki::add_field( 'my_config', array(
    'type'     => 'text',
    'settings' => 'url_simulacao',
    'label'    => __( 'Url simulação', 'evo' ),
    'section'  => 'simulacao_section',
    'default'  => esc_attr__( '', 'evo' ),
    'priority' => 10,
) );

moove_Kirki::add_section( 'contato_section', array(
    'title'          => __( 'Texto contato', 'evo' ),
    'description'    => __( 'Insira o texto de contato', 'evo' ),
    'panel'          => 'panel_contact', // Not typically needed.
    'priority'       => 160,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '', // Rarely needed.
) );
moove_Kirki::add_field( 'my_config', array(
    'type'     => 'text',
    'settings' => 'texto_contato',
    'label'    => __( 'Texto contato', 'evo' ),
    'section'  => 'contato_section',
    'default'  => esc_attr__( '', 'evo' ),
    'priority' => 10,
) );

moove_Kirki::add_section( 'phone_section', array(
    'title'          => __( 'Telefone', 'evo' ),
    'description'    => __( 'Insira o número de telefone', 'evo' ),
    'panel'          => 'panel_contact', // Not typically needed.
    'priority'       => 160,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '', // Rarely needed.
) );
moove_Kirki::add_field( 'my_config', array(
    'type'     => 'text',
    'settings' => 'phone',
    'label'    => __( 'Telefone', 'evo' ),
    'section'  => 'phone_section',
    'default'  => esc_attr__( '', 'evo' ),
    'priority' => 10,
) );
moove_Kirki::add_section( 'email_section', array(
    'title'          => __( 'E-mail', 'evo' ),
    'description'    => __( 'Insira o e-mail', 'evo' ),
    'panel'          => 'panel_contact', // Not typically needed.
    'priority'       => 160,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '', // Rarely needed.
) );
moove_Kirki::add_field( 'my_config', array(
    'type'     => 'text',
    'settings' => 'email',
    'label'    => __( 'E-mail', 'evo' ),
    'section'  => 'email_section',
    'default'  => esc_attr__( '', 'evo' ),
    'priority' => 10,
) );
moove_Kirki::add_section( 'whats_section', array(
    'title'          => __( 'WhatsApp', 'evo' ),
    'description'    => __( 'Insira o link da API', 'evo' ),
    'panel'          => 'panel_contact', // Not typically needed.
    'priority'       => 160,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '', // Rarely needed.
) );
moove_Kirki::add_field( 'my_config', array(
    'type'     => 'text',
    'settings' => 'url_whats',    
    'label'    => __( 'WhatsApp', 'evo' ),
    'section'  => 'whats_section',
    'default'  => esc_attr__( '', 'evo' ),
    'priority' => 10,
) );
/*
moove_Kirki::add_section( 'video_section', array(
    'title'          => __( 'Vídeo Chamada', 'evo' ),
    'description'    => __( 'Insira o link da Agenda de vídeo chamada', 'evo' ),
    'panel'          => 'panel_contact', // Not typically needed.
    'priority'       => 160,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '', // Rarely needed.
) );
moove_Kirki::add_field( 'my_config', array(
    'type'     => 'text',
    'settings' => 'url_video_chamada',    
    'label'    => __( 'Vídeo Chamada', 'evo' ),
    'section'  => 'video_section',
    'default'  => esc_attr__( '', 'evo' ),
    'priority' => 10,
) );
*/
moove_Kirki::add_section( 'chat_section', array(
    'title'          => __( 'Chat', 'evo' ),
    'description'    => __( 'Insira o link do Chat', 'evo' ),
    'panel'          => 'panel_contact', // Not typically needed.
    'priority'       => 160,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '', // Rarely needed.
) );
moove_Kirki::add_field( 'my_config', array(
    'type'     => 'text',
    'settings' => 'url_chat',    
    'label'    => __( 'Chat', 'evo' ),
    'section'  => 'chat_section',
    'default'  => esc_attr__( '', 'evo' ),
    'priority' => 10,
) );