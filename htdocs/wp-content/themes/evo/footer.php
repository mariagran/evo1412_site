<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the <div class="row">, <div id="wrapper" class="container"> and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package HasteStarter
 */

?>

		</div><!-- .row -->
	</div><!-- #wrapper.container -->

	<footer id="footer" role="contentinfo">
		<?php get_template_part( 'template-parts/footer', 'bottom' ); ?>
	</footer>

	<script type="text/javascript" src="https://cdn.rawgit.com/leafo/sticky-kit/v1.1.2/jquery.sticky-kit.min.js"></script>

	<?php wp_footer(); ?>

</body>
</html>
