<div id="header-top" class="d-none d-lg-block container-fluid px-0">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">

				<address>
					<div class="info-contact">
						<?php $template_url = get_template_directory_uri(); ?>
			            <?php $simulacao = get_theme_mod( 'simulacao', esc_url( '' )); ?>
			            <?php $url_simulacao = get_theme_mod( 'url_simulacao', esc_url( '' )); ?>
						<?php $texto_contato = get_theme_mod( 'texto_contato', esc_url( '' )); ?>
						<?php $phone = get_theme_mod( 'phone', esc_url( '' )); ?>
						<?php $email = get_theme_mod( 'email', esc_url( '' )); ?>
			            <?php $url_whats = get_theme_mod( 'url_whats', esc_url( '' )); ?>						
						<?php// $url_video_chamada = get_theme_mod( 'url_video_chamada', esc_html( '' )); ?>
						<?php $formulario_video_chamada = get_field( 'formulario_video_chamada'); ?>
						<?php $url_chat = get_theme_mod( 'url_chat', esc_url( '' )); ?>
						<?php $tel_clean = array(' ', '(', '-', ')', ':'); ?>
						<?php $tel = str_replace( $tel_clean, '', $phone); ?>
						
						<?php if($simulacao !=''): ?>
							<a href="<?php echo $url_simulacao; ?>">
								<button type="button" class="btn btn-primary btn-sm btn-bg-video btn-simulacao">
									<span class="icon-simulacao">
										<?php echo $simulacao; ?>
									</span>
								</button>
							</a>
						<?php endif ?>
						
						<?php if($texto_contato !=''): ?>
			            	<span class="icon-contato">
				                <?php echo $texto_contato; ?>
			            	</span>
			            <?php endif ?>
			            
						<?php if($phone !=''): ?>
							<a href="tel:+55<?php echo $tel; ?>">
								<button type="button" class="btn btn-primary btn-sm btn-bg-video btn-red">
									<span class="icon-phone">
										<img src="<?php echo $template_url . '/assets/img/icon_fone.png' ?>" alt="icon-phone"><?php echo $phone; ?>
									</span>
								</button>
							</a>
			            <?php endif ?>			            
						
						<?php if($url_whats !=''): ?>
							<a href="<?php echo $url_whats; ?>">
								<button type="button" class="btn btn-primary btn-sm btn-bg-video btn-red">
									<span class="icon-whats">
										<img src="<?php echo $template_url . '/assets/img/icon_whats.png' ?>" alt="icon-whats"><?php _e( 'Whats', 'evo' ); ?>
									</span>
								</button>
							</a>
						<?php endif ?>
						
						<?php if($formulario_video_chamada !=''): ?>
							<a href="#form-video-evo" role="button" data-toggle="modal">							
								<button type="button" class="btn btn-primary btn-sm btn-bg-video btn-red" data-direction='right'>
									<span class="icon-video">
										<img src="<?php echo $template_url . '/assets/img/icon_video.png' ?>" alt="icon-video"><?php _e( 'Agendar vídeo chamada', 'evo' ); ?>
									</span>
								</button>
							</a>
						<?php endif ?>

						<?php if($email !=''): ?>
							<a href="mailto:<?php echo $email; ?>">	
								<button type="button" class="btn btn-primary btn-sm btn-bg-video btn-red">		            
									<span class="icon-email">			               
										<img src="<?php echo $template_url . '/assets/img/icon_email.png' ?>" alt="icon-email"><?php _e( 'E-mail', 'evo' ); ?>				                
									</span>
								</button>
							  </a>
						<?php endif ?>
						
						<?php if($url_chat !=''): ?>
							<a href="<?php echo $url_chat; ?>">
								<button type="button" class="btn btn-primary btn-sm btn-bg-video btn-red btn-chat">
									<span class="icon-chat">
										<img src="<?php echo $template_url . '/assets/img/icon_chat.png' ?>" alt="icon-chat"><?php _e( 'Chat', 'evo' ); ?>
									</span>
								</button>
							</a>
			            <?php endif ?>
			        </div>
		        </address>

			</div>

		</div>

	</div>
</div>