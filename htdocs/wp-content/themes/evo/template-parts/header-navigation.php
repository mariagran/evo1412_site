<nav id="navigation-top" class="navbar navbar-expand-md navbar-light bg-light col-md-9 col-sm-12" role="navigation">
	<div class="container-menu">
		
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="main-menu">
			<?php
				wp_nav_menu( array(
						'theme_location' => 'main-menu',
						'depth'          => 2,
						'container'      => false,
						'menu_class'     => 'navbar-nav float-md-right',
						'fallback_cb'    => 'Haste_Starter_Bootstrap_Nav_Walker::fallback',
						'walker'         => new Haste_Starter_Bootstrap_Nav_Walker(),
				) );
			?>
		</div><!-- .navbar-collapse -->
	</div><!-- .container -->
</nav>
