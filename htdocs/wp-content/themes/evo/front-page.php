<?php
/**
 * Template Name: Home Page
 *
 * The template for displaying in Front Page.
 *
 * @package Odin
 * @since 2.2.0
 */

get_header(); ?>

<main id="content" class="<?php echo haste_starter_classes_page_full(); ?> px-0" tabindex="-1" role="main">

	<?php

	if( have_rows('conteudo_home') ):

		while ( have_rows('conteudo_home') ) : the_row();

			if( get_row_layout() == 'banner' ): ?>

				<?php $id_banner = get_sub_field( 'id_ancora' ); ?>

				<section id="<?php echo $id_banner; ?>" class="text-center container-fluid px-0 slider-home">

					<div class="banner-box">

						<?php
						$image = wp_get_attachment_image_src(get_sub_field('imagem', $post->ID), 'icon-home');
						$alt_text = get_sub_field( 'imagem', $post->ID );
						$alt = get_post_meta($alt_text, '_wp_attachment_image_alt', true);
						$link = get_sub_field( 'link_banner' );
						?>

						<?php if ( get_sub_field( 'imagem' )): ?>

							<figure>

								<?php if ( $link != '' ): ?>

									<a href="<?php echo $link; ?>">

										<img src="<?php echo $image[0]; ?>" alt="<?php echo $alt; ?>">
										
									</a>

								<?php else: ?>

									<img src="<?php echo $image[0]; ?>" alt="<?php echo $alt; ?>">

								<?php endif; ?>

							</figure>

						<?php endif; ?>

					</div>

					<?php wp_reset_postdata(); ?>

				</section>

			<?php

			elseif( get_row_layout() == 'video' ): ?>

				<?php $id_banner = get_sub_field( 'id_ancora' ); ?>

				<?php 

				$titulo = get_sub_field( 'titulo' );
				$id_ancora = get_sub_field( 'id_ancora' );
				$slogan = get_sub_field( 'slogan' );
				$texto_botao = get_sub_field( 'texto_botao' );
				$url_video = get_sub_field( 'url_video' );

				if ( $titulo != '' ): 

					?>

					<section id="<?php echo $id_banner; ?>" class="text-center container-fluid px-0 background-video">

						<div class="container container-movimento px-0">							

							<h1 class="titulo-video"><?php echo $titulo; ?></h1>
							<div class="slogan-video"><?php echo $slogan; ?></div>

						</div>

						<div class="container container-btn">
								
							<div id="<?php echo $id_ancora; ?>" class="bg-image-video video-evo" role="contentinfo">

								<a href="#video-evo" role="button" data-toggle="modal">

									<!-- Button trigger modal -->

									<button type="button" class="btn btn-primary btn-sm btn-bg-video" data-direction='right'>
										<?php echo $texto_botao; ?>
									</button>

								</a>

							</div>							

						</div>

						<?php wp_reset_postdata(); ?>

					</section>
				
				<?php endif; ?>																

			<?php

			elseif( get_row_layout() == 'carrossel' ): ?>

			<?php $id_carrossel = get_sub_field( 'id_ancora' ); ?>

				<section id="<?php echo $id_carrossel; ?>" class="container-carrossel" >

					<?php $shortcode_carrossel = get_sub_field('shortcode_carrossel'); ?>

					<?php echo $shortcode_carrossel; ?>

					<?php wp_reset_postdata(); ?>

				</section>	
				
				
			<section class="container-plantas-e-localizacao" >

				<div class="container">

					<div class="row row-plantas-e-localizacao">

						<div class="col-lg-7">

							<?php

							elseif( get_row_layout() == 'plantas' ): ?>				

								<?php $id_plantas = get_sub_field( 'id_ancora_plantas' ); ?>

								<div id="<?php echo $id_plantas; ?>">

									<?php 
									$texto_plantas = get_sub_field('texto_plantas');									
									$images = get_sub_field('imagens_plantas'); ?>

									<div class="conteudo-plantas">

										<?php echo $texto_plantas; ?>									

									</div><!-- .conteudo-plantas -->

									<?php
									
									if( $images ): 					
									
									?>
										<div class="text-center gallery">
											<ul class="row">
												<?php foreach( $images as $image ): 
													$content = '<li class="col-6 col-sm-6 col-md-4 img-galeria">';
														$content .= '<a class="gallery_image" href="'. $image['url'] .'">';
															$content .= '<div class="overlay"></div>';
															$content .= '<img src="'. $image['sizes']['img-gallery-3col'] .'" alt="'. $image['alt'] .'" />';
															$content .= '<p class="p-caption">'. $image['title'] .'</p>';
															$content .= '<p class="p-caption">'. $image['caption'] .'</p>';
														$content .= '</a>';
													$content .= '</li>';
													if ( function_exists('slb_activate') ){
														$content = slb_activate($content);
														}
														
													echo $content;?>
												<?php endforeach; ?>
											</ul>
										</div><!-- .gallery -->

									<?php endif; ?>

								</div><!-- id_plantas -->

								<?php wp_reset_postdata(); ?>

							<?php

							elseif( get_row_layout() == 'localizacao' ): ?>

							<hr>

							<?php
								$id_localizacao = get_sub_field( 'id_ancora_localizacao' ); 
								$texto_localizacao = get_sub_field('texto_localizacao');
							?>

							<div id="<?php echo $id_localizacao; ?>">

								<div class="conteudo-localizacao">

									<?php echo $texto_localizacao; ?>

									<?php

									if ( have_rows( 'estabelecimentos_proximos' ) ) : ?>

										<div class="row">												

											<?php

											while ( have_rows( 'estabelecimentos_proximos' ) ) : the_row(); ?>

												<div class="col-6">

													<?php

													$icone = wp_get_attachment_image_src(get_sub_field('icone', $post->ID), 'full');
													$alt_text = get_sub_field( 'icone', $post->ID );
													$alt = get_post_meta($alt_text, '_wp_attachment_image_alt', true);
													$texto = get_sub_field('texto');
													?>

													<?php if ( $texto != '' ): ?>

														<div class="row row-estabelecimentos">
															<div class="col-3 col-icone">
																<img src="<?php echo $icone[0]; ?>" alt="<?php echo $alt; ?>">
															</div>
															<div class="col-9 col-texto">
																<?php echo $texto; ?>
															</div>
														</div>

													<?php endif; ?>

												</div><!-- .col-6 -->

											<?php endwhile; ?>												

										</div><!-- .row -->

									<?php

									endif; ?>										

								</div><!-- .conteudo-localizacao -->

							</div><!-- id_localizacao -->

						</div><!-- .col-lg-7 -->	

						<?php

						elseif( get_row_layout() == 'formulario_lateral' ): ?>

						<?php $shortcode_contato_lateral = get_sub_field( 'shortcode_contato_lateral' ); ?>

						<div id="sidebar-contato" class="col-sm-12 col-lg-5">

							<div id="contato-sidebar">

								<div id="sidebar-form">

									<?php echo $shortcode_contato_lateral; ?>	

								</div><!-- #sidebar-form -->								
								
							</div><!-- #contato-sidebar -->

						</div><!-- #sidebar-contato -->

					</div><!-- .row-plantas-e-localizacao -->

				</div><!-- .container -->

				<?php wp_reset_postdata(); ?>

			</section>
				
			<?php elseif( get_row_layout() == 'mapa' ): ?>

			<?php
				$id_mapa = get_sub_field( 'id_ancora' ); 
				$html_mapa = get_sub_field( 'html_mapa' );
			?>

			<section id="<?php echo $id_mapa; ?>" class="section-mapa">

				<div class="container-fluid px-0">

					<div class="row mx-0">

						<div id="mapa-evo" class="col-sm-12 px-0" role="complementary">
							<?php
								echo $html_mapa;
							?>
						</div>

					</div>

				</div>

				<?php wp_reset_postdata(); ?>

			</section>
			
			<?php elseif( get_row_layout() == 'diferenciais' ): ?>

			<?php
				$id_diferenciais = get_sub_field( 'id_ancora' ); 
				$titulo = get_sub_field( 'titulo' );
			?>

			<section id="<?php echo $id_diferenciais; ?>">

				<div class="container">

					<div class="row titulo-diferenciais">

						<h1><?php echo $titulo; ?></h1>

					</div>

					<?php

					if ( have_rows( 'conteudo_diferenciais' ) ) : ?>

						<div class="row row-diferenciais">												

							<?php

							while ( have_rows( 'conteudo_diferenciais' ) ) : the_row(); ?>

								<div class="col-sm-12 col-md-6 col-lg-3 col-diferenciais">

									<?php

									$icone = wp_get_attachment_image_src(get_sub_field('icone', $post->ID), 'full');
									$alt_text = get_sub_field( 'icone', $post->ID );
									$alt = get_post_meta($alt_text, '_wp_attachment_image_alt', true);
									$titulo = get_sub_field('titulo');
									$texto = get_sub_field('texto');
									?>

									<?php if ( $icone != '' ): ?>
											
										<img src="<?php echo $icone[0]; ?>" alt="<?php echo $alt; ?>">												

									<?php endif; ?>

									<div class="diferenciais">

										<?php if ( $titulo != '' ): ?>

											<h3><?php echo $titulo; ?></h3>

										<?php endif; ?>

										<?php if ( $texto != '' ): ?>

											<div class="conteudo-diferenciais"><?php echo $texto; ?></div>

										<?php endif; ?>

									</div>

								</div>

							<?php endwhile; ?>												

						</div>

					<?php

					endif;?>


				</div>

				<?php wp_reset_postdata(); ?>

				<hr>

			</section>

			<?php elseif( get_row_layout() == 'galeria' ): ?>

				<?php
				$id_galeria = get_sub_field( 'id_ancora' );
				$texto_galeria = get_sub_field( 'texto_galeria' );
				$imagens_galeria = get_sub_field('imagens_galeria'); ?>

				<?php

				if( $imagens_galeria ): ?>

					<section id="<?php echo $id_galeria; ?>" class="text-center">

						<div class="container container-texto texto-galeria">							

							<?php echo $texto_galeria; ?>

						</div>

						<div class="container gallery">
							<ul class="row">
								<?php foreach( $imagens_galeria as $imagens_galeria ): 
									$content_galeria = '<li class="col-6 col-sm-6 col-md-4 col-lg-20 img-galeria">';
										$content_galeria .= '<a class="gallery_image" href="'. $imagens_galeria['url'] .'">';
											$content_galeria .= '<div class="overlay"></div>';
											$content_galeria .= '<img src="'. $imagens_galeria['sizes']['img-gallery-3col'] .'" alt="'. $imagens_galeria['alt'] .'" />';
											$content_galeria .= '<p class="p-caption">'. $imagens_galeria['title'] .'</p>';
										$content_galeria .= '</a>';
									$content_galeria .= '</li>';
									if ( function_exists('slb_activate') ){
										$content_galeria = slb_activate($content_galeria);
										}
										
									echo $content_galeria;?>
								<?php endforeach; ?>
							</ul>
						</div>

						<?php wp_reset_postdata(); ?>

					</section>

				<?php endif; ?>
				
			<?php

			elseif( get_row_layout() == 'banner_texto' ): ?>

				<?php 

				$texto_banner = get_sub_field( 'texto' );
				$id_banner_texto = get_sub_field( 'id_ancora' );

				if ( $texto_banner != '' ): 

					?>

					<section id="<?php echo $id_banner_texto; ?>" class="text-center container-fluid px-0 banner-texto">

						<div class="container container-texto-banner">							

							<?php echo $texto_banner; ?>

						</div>

						<?php wp_reset_postdata(); ?>

					</section>
				
				<?php endif; ?>	
				
			<?php

			elseif( get_row_layout() == 'sobre' ): ?>

				<?php 

				$texto_sobre = get_sub_field( 'texto' );
				$id_sobre = get_sub_field( 'id_ancora' );

				?>

				<section id="<?php echo $id_sobre; ?>" class="text-center container-fluid sobre">

					<div class="container container-texto-sobre">
						
						<div class="row row-sobre">

							<div class="col-lg-7">

							<?php

							if ( have_rows( 'logos' ) ) : ?>

								<div class="row row-logos">												

									<?php

									while ( have_rows( 'logos' ) ) : the_row(); ?>

										<div class="col-6">

											<?php

											$icone = wp_get_attachment_image_src(get_sub_field('imagem', $post->ID), 'full');
											$alt_text = get_sub_field( 'imagem', $post->ID );
											$alt = get_post_meta($alt_text, '_wp_attachment_image_alt', true);
											$link = get_sub_field('link');
											?>

											<?php if ( $icone != '' ): ?>

												<a href="<?php echo $link; ?>">
													
													<img src="<?php echo $icone[0]; ?>" alt="<?php echo $alt; ?>">
													
												</a>

											<?php endif; ?>

										</div>

									<?php endwhile; ?>												

								</div>

							<?php

							endif;?>

							</div>

							<div class="col-lg-5">

								<?php if ( $texto_sobre != '' ): ?>

									<?php echo $texto_sobre; ?>

								<?php endif; ?>

							</div>

						</div>

					</div>

					<?php wp_reset_postdata(); ?>

					<hr>

				</section>
				
			<?php

			elseif( get_row_layout() == 'contato' ): ?>

				<?php 

				$shortcode_formulario = get_sub_field( 'shortcode_formulario' );
				$id_contato = get_sub_field( 'id_ancora' );

				if ( $shortcode_formulario != '' ): 

					?>

					<section id="<?php echo $id_contato; ?>" class="text-center container-fluid shortcode-formulario">

						<div class="container container-shortcode-formulario">							

							<?php echo $shortcode_formulario; ?>

						</div>

						<?php wp_reset_postdata(); ?>

					</section>
				
				<?php endif; ?>

			<?php

			elseif( get_row_layout() == 'bloco_texto' ): ?>

				<?php 

				$texto_bloco = get_sub_field( 'texto_bloco' );
				$id_bloco_texto = get_sub_field( 'id_ancora' );

				if ( $texto_bloco != '' ): 

					?>

					<section id="<?php echo $id_bloco_texto; ?>" class="text-center container-fluid">

						<div class="container container-texto texto-design">							

							<?php echo $texto_bloco; ?>

						</div>

						<?php wp_reset_postdata(); ?>

					</section>
				
				<?php endif; ?>	

			<?php

			endif;

			endwhile;

		else :

		// no layouts found

	endif;

	?>

</main><!-- #main -->

<!-- Modal -->

<?php

if( have_rows('conteudo_home') ): ?>

	<?php while ( have_rows('conteudo_home') ) : the_row(); ?>

		<?php if( get_row_layout() == 'video' ): ?>

			<?php $video = get_sub_field( 'url_video'); ?>

			<?php if( $video != '' ): ?>

				<div class="modal fade right" id="video-evo" tabindex="-1" role="dialog" aria-labelledby="depoimentoTitle" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body embed-container">

								<?php echo $video; ?>

							</div>
						</div>
					</div>
				</div>


			<?php endif; ?>

		<?php endif; ?>

	<?php endwhile; ?>

<?php endif; ?>

<!-- Modal Formulário Vídeo Chamada -->

<?php $formulario_video_chamada = get_field( 'formulario_video_chamada'); ?>
<?php // $formulario_video_chamada = get_theme_mod( 'url_video_chamada', esc_html( '' ) ); ?>

<?php if( $formulario_video_chamada != '' ): ?>

	<div class="modal modal-form fade right" id="form-video-evo" tabindex="-1" role="dialog" aria-labelledby="videoTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body embed-container">
				
					<?php 
						$short = "'$formulario_video_chamada'";
						echo do_shortcode($short); 
					?>

				</div>
			</div>
		</div>
	</div>

<?php endif; ?>

<?php
get_footer();