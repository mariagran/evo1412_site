var $ = jQuery.noConflict();

/* STOP VIDEO IN MODAL */
$("#video-boat").on('hidden.bs.modal', function (e) {
  $("#video-boat video").attr("src", $("#video-boat video").attr("src"));
});

$("#video-boat").on('hidden.bs.modal', function (e) {
  $("#video-boat iframe").attr("src", $("#video-boat iframe").attr("src"));
});


$(document).ready(function(){
  $('.modal').each(function(){
          var src = $(this).find('iframe').attr('src');

      $(this).on('click', function(){

          $(this).find('iframe').attr('src', '');
          $(this).find('iframe').attr('src', src);

      });
  });

  /* SIDEBAR FIXED */
    $("#contato-sidebar").stick_in_parent({
        inner_scrolling: false,
        offset_top: 120,
    });

  
});

/* SMOOTH SCROLL IN MENU TOUR */
var $ = jQuery.noConflict()

  $('a[href*=\\#]:not([href=\\#video-evo]):not([href=\\#form-video-evo])').click(function() {
	if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
		|| location.hostname == this.hostname) {

		var target = $(this.hash);
		target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		   if (target.length) {
			 $('html,body,navigation-top').animate({
				 //scrollTop: target.offset().top
				 scrollTop: (($ (target) .offset (). top) - 180)
			}, 500);
			return false;
		}
	}
});